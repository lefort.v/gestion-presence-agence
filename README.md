# :calendar: Gestion des présences à l'agence

## :information_source: Description

Application pour que chacun puisse simplement renseigner ses jours de présence à l'agence et partager l'information dans
ses différents calendriers.

## Infos générales

### :warning: API Key Google

:warning: Pour le moment, le projet est sous une API de du compte de Valentin Lefort, pour pouvoir utiliser
l'application, il faut demander l'accès (en mode test, on peut ajouter jusqu'à 100 personnes donc normalement pas de
problème de place)

### :wrench: Dépendences

- VueJS
    - [Vuetify](https://vuetifyjs.com/en/) (Bibliothèque de composants graphiques Material)
    - [Vue-Router](https://router.vuejs.org/)
    - [Vuex](https://vuex.vuejs.org/) pour la gestion des stores
    - [Axios](https://axios-http.com/) pour les requêtes http
    - [Moment](https://momentjs.com/) pour la gestion des dates
    - [VueGapi](https://github.com/vue-gapi/vue-gapi/tree/releases/v1) pour la gestion de la connexion aux services
      Google

## :arrow_forward: Démarrage en local (avec le backend Hasura)

### Démarrage du backend

La commande `docker-compose up` à partir du `docker-compose.yml` present dans `/docker/hasura-graphql` permet de créer :

- Une base de données [Postgresql](https://www.postgresql.org/) qui contient
    - Deux tables (`Utilisateur` et `Presence`)
    - Une fonction `get_presents_du_jour` pour lister les personnes présentes en fonction de la date
    - Une vue `presences_par_jour` qui donne pour chaque date la liste des personnes présentes
- Un moteur [GraphQL](https://graphql.org/) Hasura qui se connecte à la bdd
    - Hasura expose une console d'administration sur http://locahost:9000
    - Par défaut, les tables, vues et fonctions ne sont pas exposées par l'API GraphQL pour que ça fonctionne (TODO: A
      automatiser), il faut :
        - Se connecter sur la console d'admin
        - Aller sur l'onglet DATA
        - Cliquer sur 'Track' pour tous les objets dont on a besoin.
- Quelques données de test

### Démarrage du front

Lancer la commande `npm run serve` à la racine du projet vuejs lance l'appli sur le port `8080`

## Vidéo démo

![Démo](docs/demo-syncho.mp4)