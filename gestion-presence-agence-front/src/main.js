import Vue from 'vue'
import App from './App.vue'
import vuetify from './plugins/vuetify'
import router from './router'
import store from './store'
import VueGapi from "vue-gapi";

Vue.config.productionTip = false

Vue.use(VueGapi, {
    client_id: '946925880251-rj9j1g3fqj6qah1dlfufrc21gomv7bd5.apps.googleusercontent.com',
    scope: 'https://www.googleapis.com/auth/calendar'
})


new Vue({
    vuetify,
    router,
    store,
    render: h => h(App)
}).$mount('#app')
