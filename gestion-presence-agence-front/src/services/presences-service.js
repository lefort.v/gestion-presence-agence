const axios = require('axios')

const GRAPHQL_URL = "http://localhost:9000/v1/graphql"

function graphql_call(data) {
    return axios({
        method: 'post',
        url: GRAPHQL_URL,
        data:
        data
    })
}

export default {
    getPresencesForDate(date) {
        return graphql_call(
            {
                query: `query { presence (where: {date_presence:{_eq:"` + date + `"}}) { type_presence utilisateur { a_la_cle id_google photo fullname }}}`
            }
        ).then(response => {
            return response.data.data.presence
        })
    },
    addUser(googleUser, a_la_cle) {
        return graphql_call({
            query: `mutation {
                       insert_utilisateur_one(
                           object: {
                                id_google: "` + googleUser.id + `",
                                fullname: "` + googleUser.fullName + `",
                                photo: "` + googleUser.imageUrl + `",
                                a_la_cle: ` + a_la_cle + `
                            },
                           on_conflict: { constraint: utilisateur_id_google_key, update_columns: [fullname, photo]}) {
                            id_google
                            fullname
                            a_la_cle
                            photo
                       }
                   }`
        }).then(response => {
            return response.data.data.insert_utilisateur_one
        })
    },
    addPresence(infos) {
        return graphql_call({
            query: `mutation {
                        insert_presence_one(object: {date_presence: "` + infos.date + `", id_utilisateur: "` + infos.id_google + `", type_presence: "` + infos.type_presence + `"}) {
                            id
                        }
                    }`
        })

    },
    removePresence(infos) {
        return graphql_call({
            query: `mutation {
                        delete_presence(where: {id_utilisateur: {_eq: "` + infos.id_google + `"}, date_presence: {_eq: "` + infos.date + `"}}) {
                            affected_rows
                        }
                    }`
        })
    },
    getUser(id_google) {
        return graphql_call({
            query: `{
                      utilisateur(where: {id_google: {_eq: "` + id_google + `"}}) {
                        id_google fullname photo a_la_cle
                      }
                    }`
        }).then(response => {
            const users = response.data.data.utilisateur
            if (users.length === 1) {
                return users[0]
            }
            return {}
        })
    },
    updateNewUserInfos(infos) {
        return graphql_call({
            query: `mutation {
  update_utilisateur(where:{id_google:{_eq:"` + infos.id_google + `"}}, _set: {a_la_cle : ` + infos.aLaCle + `}) {
    affected_rows
  }
}`
        })
    },
    updateCalendarSynchro(infos) {
        return graphql_call({
            query: `mutation {
                        insert_synchro_calendrier_one(object: {id_calendrier: "` + infos.id_calendrier + `", id_utilisateur: "` + infos.id_google + `", synchro: ` + infos.synchro + `}, on_conflict: {constraint: synchro_calendrier_pkey, update_columns: synchro}) {synchro}
                    }`
        })
    },
    getCalendarSynchro(user_id, id_calendrier) {
        return graphql_call({
            query: `{
  synchro_calendrier(where: {id_calendrier: {_eq: "` + id_calendrier + `"}, id_utilisateur: {_eq: "` + user_id + `"}}) {
    synchro
  }
}`
        }).then(response => {
            if (response.data.data.synchro_calendrier[0]) {
                return response.data.data.synchro_calendrier[0].synchro
            } else {
                return false
            }
        });
    },
    getSynchronizedCalendars(user_id) {
        return graphql_call({
            query: `{
  synchro_calendrier(where: {_and: {id_utilisateur: {_eq: "` + user_id + `"}, synchro: {_eq: true}}}) {
    synchro
    id_calendrier
  }
}
`
        }).then(response => response.data.data.synchro_calendrier)
    }
}