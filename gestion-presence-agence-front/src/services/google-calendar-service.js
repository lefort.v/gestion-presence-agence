import moment from "moment";

export default {
    listerLesCalendriers(gapiClient) {
        return gapiClient.then(
            (response) => {
                return response.client.request({
                    'path': 'https://www.googleapis.com/calendar/v3/users/me/calendarList',
                }).then(function (response) {
                    return response.result.items.filter(calendrier => calendrier.accessRole === "owner");
                }, function (reason) {
                    console.log('Error: ' + reason.result.error.message);
                });
            }
        )
    },
    envoyerEvenement(gapiClient, idCalendrier, event) {
        gapiClient.then(
            (response) => {
                return response.client.request({
                    'method': 'post',
                    'path': 'https://www.googleapis.com/calendar/v3/calendars/' + idCalendrier + '/events',
                    body: event
                }).then(function (response) {
                    console.log(response.result);
                }, function (reason) {
                    console.log('Error: ' + reason.result.error.message);
                });
            }
        )
    },
    listerEvenements(gapiClient, id_calendrier, date) {
        return gapiClient.then(
            (response) => {
                return response.client.request({
                    'method': 'get',
                    'path': 'https://www.googleapis.com/calendar/v3/calendars/' + id_calendrier + '/events?timeMin=' + date + 'T00:00:00Z&timeMax=' + moment(date).add(1, "day").format("YYYY-MM-DD") + 'T00:00:00Z'
                }).then(function (response) {
                    console.log(response.result);
                    return response.result.items.filter(event => event.summary.includes("auto-présence :"))
                }, function (reason) {
                    console.log('Error: ' + reason.result.error.message);
                });
            }
        )
    },
    supprimerEvenement(gapiClient, id_calendrier, id_event) {
        gapiClient.then(
            (response) => {
                return response.client.request({
                    'method': 'delete',
                    'path': 'https://www.googleapis.com/calendar/v3/calendars/' + id_calendrier + '/events/' + id_event
                }).then(function (response) {
                    console.log(response.result);
                }, function (reason) {
                    console.log('Error: ' + reason.result.error.message);
                });
            }
        )
    },
}