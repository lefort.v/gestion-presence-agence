import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import store from "@/store"
import Login from "@/views/Login";
import Weekly from "@/views/Weekly";

Vue.use(VueRouter)

const routes = [
    {
        path: '/login',
        name: 'Login',
        component: Login
    },
    {
        path: '/home',
        name: 'Home',
        component: Home,
        beforeEnter: (to, from, next) => {
            if (store.getters.isAuthenticated) {
                next()
            } else {
                next("/login")
            }
        }
    },
    {
        path: '/week',
        name: 'Weekly',
        component: Weekly,
        beforeEnter: (to, from, next) => {
            if (store.getters.isAuthenticated) {
                next()
            } else {
                next("/login")
            }
        }
    },
    {
        path: "*",
        redirect: "Login"
    }
]

const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes
})

export default router
