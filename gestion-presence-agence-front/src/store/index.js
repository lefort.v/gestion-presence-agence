import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        authenticated: false,
        currentUser: {}
    },
    getters: {
        isAuthenticated(state) {
            return state.authenticated;
        },
        currentUser(state) {
            return state.currentUser
        }
    },
    mutations: {
        authentify(state) {
            state.authenticated = !state.authenticated
        },
        setCurrentUser(state, user) {
            state.currentUser = user
        }
    },
    actions: {
        authentify(context) {
            context.commit('authentify')
        },
        setCurrentUser(context, user) {
            context.commit('setCurrentUser', user)
        }
    },
    modules: {}
})
