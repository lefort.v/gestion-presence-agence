DROP VIEW IF EXISTS presences_par_jour;
DROP FUNCTION IF EXISTS get_presents_du_jour;
DROP TABLE IF EXISTS PRESENCE;
DROP TABLE IF EXISTS SYNCHRO_CALENDRIER;
DROP TABLE IF EXISTS UTILISATEUR;


CREATE TABLE UTILISATEUR
(
    ID_GOOGLE VARCHAR UNIQUE,
    FULLNAME  VARCHAR,
    PHOTO     VARCHAR,
    A_LA_CLE  BOOLEAN
);

CREATE TABLE PRESENCE
(
    ID             INT UNIQUE GENERATED ALWAYS AS IDENTITY,
    ID_UTILISATEUR varchar,
    DATE_PRESENCE  VARCHAR,
    TYPE_PRESENCE  VARCHAR,
    CONSTRAINT fk_utilisateur
        FOREIGN KEY (ID_UTILISATEUR)
            REFERENCES UTILISATEUR (ID_GOOGLE)
);

create function get_presents_du_jour(jour character varying) returns SETOF utilisateur
    stable
    language plpgsql
as
$$
BEGIN
    RETURN QUERY
        SELECT util.*
        FROM PRESENCE pres
                 INNER JOIN utilisateur util ON pres.id_utilisateur = util.ID_GOOGLE
        WHERE pres.date_presence = jour;

end;
$$;

create view presences_par_jour(date_presence, presents) as
SELECT pres.date_presence,
       array_agg(util.fullname) AS presents
FROM presence pres
         JOIN utilisateur util ON pres.id_utilisateur = util.ID_GOOGLE
GROUP BY pres.date_presence;

CREATE TABLE SYNCHRO_CALENDRIER
(
    ID_UTILISATEUR varchar,
    ID_CALENDRIER  VARCHAR,
    SYNCHRO  BOOLEAN,
    PRIMARY KEY (ID_UTILISATEUR,ID_CALENDRIER),
    CONSTRAINT fk_utilisateur
        FOREIGN KEY (ID_UTILISATEUR)
            REFERENCES UTILISATEUR (ID_GOOGLE)
);
